Feature: Search for the product

  @Positive
  Scenario Outline: Search for existing products
    When he calls endpoint "/<product>"
    Then he sees the results displayed for <product>
    Examples:
      | product |
      | apple   |
      | mango   |
      | tofu    |
      | water   |

  @NotExistingProduct
  Scenario: Not existing endpoint
    When he calls endpoint "/car"
    Then he does not see the results

  @MethodNotAllowed
  Scenario: Not allowed http method
    When he calls endpoint "/apple" with post
    Then he is not allowed to do that

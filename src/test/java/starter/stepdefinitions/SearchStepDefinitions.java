package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;
import org.hamcrest.Matchers;
import org.hamcrest.core.Every;

import java.io.File;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;


public class SearchStepDefinitions {

    private EnvironmentVariables environmentVariables;

    @When("he calls endpoint {string}")
    public void he_calls_endpoint(String product) {
        SerenityRest.given().get(environmentVariables.getProperty("restapi.baseUrl") + product);
    }

    @When("he calls endpoint {string} with post")
    public void he_calls_endpoint_post(String product) {
        SerenityRest.given().post(environmentVariables.getProperty("restapi.baseUrl") + product);
    }

    @Then("he sees the results displayed for {word}")
    public void he_Sees_the_results_displayed_for_mango(String product) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("$.title", Every.everyItem(Matchers.containsStringIgnoringCase(product))));
        File schema = new File(System.getProperty("user.dir")+"/src/test/resources/jsonSchemas/results.json");
        restAssuredThat(response -> response.body(
                matchesJsonSchema(schema)));
    }

    @Then("he does not see the results")
    public void he_doesn_not_see_the_results() {
        restAssuredThat(response -> response.statusCode(404));
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

    @Then("he is not allowed to do that")
    public void he_is_not_allowed_to_do_that() {
        restAssuredThat(response -> response.statusCode(405));
        restAssuredThat(response -> response.body("detail", equalTo("Method Not Allowed")));
    }
}
